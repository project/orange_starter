
/**
 * Initializes the AOS library.
 */
(function ($, Drupal) {

  AOS.init();

})(jQuery, Drupal);
